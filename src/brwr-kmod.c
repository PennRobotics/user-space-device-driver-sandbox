/// #include <linux/...>  // TODO
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/usb/input.h>
#include <stdlib.h>  // TODO: replace?
#define  DRIVER_AUTHOR  "Brian Wright <brianwright@alumni.upenn.edu>"
#define  DRIVER_DESC    ""

static struct usb_device *dev;
static struct usb_device_id dev_table[] = {
    {USB_DEVICE(0x####, 0x####)},
    /* TODO? USB_DEVICE_AND_INTERFACE_INFO(...) */
    /* TODO? .match_flags */
    {}
};
MODULE_DEVICE_TABLE(usb, dev_table);

// TODO: create and name todo_driver

static int dev_debug(struct usb_interface *intf, const struct usb_device_id *id)
{
    struct usb_device *dev = interface_to_usbdev(intf);
    int error;
    // TODO: ...

    // TODO: get interface

    // TODO: check num of endpoints

    // TODO: setup

    // TODO: register device (also sets error variable)

    printk(KERN_ALERT "Test KERN_ALERT message: GHJKL")
    pr_info("Test message GHJKL\n");

    return error;
}


static void dev_disconnect(struct usb_interface *interface)
{
    printk(KERN_ALERT "USB disconnecting\n");
    usb_deregister_dev(interface, NULL);
    /// if (registered)
    ///     /* TODO */
    /// registered = 0;
}


static int __init usb_init(void)
{
    int error;
    printk(KERN_ALERT "USB driver loaded into kernel tree\n");
    error = usb_register(&todo_driver);  // TODO: name
    if (error)
        printk(KERN_ALERT "usb_register failed. Error code %xh\n", error);
    return error;
}


static void __exit usb_exit(void)
{
    usb_deregister(&todo_driver);  // TODO: name
}

module_init(usb_init);
module_exit(usb_exit);
