#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Box.H>


int main(int argc, char **argv) {
    Fl_Window *window = new Fl_Window(300, 200);

    Fl_Box *box = new Fl_Box(20, 40, 200, 100, "Test");
    box->box(FL_UP_BOX);
    box->labelsize(32);
    box->labeltype(FL_SHADOW_LABEL);

    window->end();
    window->show(argc, argv);

    return Fl::run();
}