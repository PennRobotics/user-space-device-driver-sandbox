# User Space Device Driver Sandbox

_exploration environment for reverse engineering or cleanroom development of Linux drivers for various (likely USB) peripherals: camera, pro audio, human input, sensors, etc._


## Build / Install

### dependencies
TODO

### make
TODO

### clean
TODO

### run
TODO

### install
TODO


## Tasks

- [ ] _Linux Device Drivers_ book

-----

- [ ] look for SDKs and existing efforts
- [ ] unpack existing drivers
- [ ] get low-hanging fruit, i.e. using `strings` on libraries
- [ ] get device IDs (VID/PID in Linux, UUID in Windows)
- [ ] fix permissions via **udev**
- [ ] USB sniffing of working driver on other OS (Wireshark?)
- [ ] `binwalk`
- [ ] decide which endpoint(s) to use
    - control
        - 8, 16, 32, or 64 bytes per packet (speed-dependent)
        - "setup" stage, 3 packets (token=addr+EP#, data/setup packet, handshake)
        - "data" stage (opt.), variable I/O
        - "status" stage, acknowledge received data and examine sent data response
        - setup tokens  &rarr;  Sync, PID, ADDR, ENDP, CRC5, EOP
        - data0 packet  &rarr;  Sync, PID, Data0, CRC16, EOP
        - ack handshake &rarr;  Sync, PID, EOP
        -   &hellip;
    - interrupt
        - _latency guaranteed, unidirectional, uses error detection and retries_
        - speed-dependent 8-, 64-, or 1024-byte max data payload
    - isochronous
        - _continuous, periodic, bounded latency, unidirectional, no retries, ideal for audio/video_
        - speed-dependent 1023- or 1024-byte max payload
    - bulk
        - _bursts of large data, uses spare bandwidth, error correction and retries_
        - 8|16|32|64-byte for full speed / up to 512 for high speed
- [ ] `perf` ?
- [ ] Windows in a VM, breakpoints, inspect hardware and/or registers before/after driver calls


## What's sitting around the house?

- Big audio interface
- Stereo audio interface
- Mono audio interface
- Instrument audio interface
- MIDI controller
- Microcontrollers
- Mirrorless camera
- Kids camera


## References / Links

- https://github.com/frank26080115/alpha-fairy
- https://github.com/keyserSoze42/SonySDK
- https://blog.benjojo.co.uk/post/userspace-usb-drivers

### unvetted
- https://voidsec.com/windows-drivers-reverse-engineering-methodology/
- https://github.com/torvalds/linux/blob/master/drivers/usb/usb-skeleton.c
- https://github.com/pwr-Solaar/Solaar
- https://github.com/mellowcandle/bellwin_usb_linux_driver
- https://github.com/ishank62/USB-Device-Driver/blob/master/stick_driver.c
- https://github.com/DIGImend/digimend-kernel-drivers
- https://github.com/wjasper/Linux_Drivers
- https://github.com/Print3M/pyusb-katana-driver
- https://github.com/wheaney/xrealAirLinuxDriver
- https://github.com/biagiop1986/fast_driver
- https://github.com/hselasky/webcamd
- https://github.com/ayushabrol13/USB-Device-Driver
- https://www.youtube.com/watch?v=j5NciKpHZzs
- https://serhack.me/articles/reverse-engineering-omnivision-os12d40-driver/
- https://web.archive.org/web/20201116224520/https://www.linuxvoice.com/drive-it-yourself-usb-car-6/
- https://www.thegeekstuff.com/2013/07/write-linux-kernel-module/
